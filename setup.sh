#!/bin/sh

TARGET=$1

perl /usr/share/multistrap/device-table.pl -d $TARGET -f device_list.txt --no-fakeroot

echo \
"#!/bin/sh
exit 101" > "$TARGET/usr/sbin/policy-rc.d"
chmod 755 "$TARGET/usr/sbin/policy-rc.d"

echo \
"#!/bin/sh
echo
echo \"Warning: Fake start-stop-daemon called, doing nothing\"" > "$TARGET/sbin/start-stop-daemon"
chmod 755 "$TARGET/sbin/start-stop-daemon"

echo Etc/UTC > "$TARGET/etc/timezone"
ln -s /usr/share/zoneinfo/Etc/UTC "$TARGET/etc/localtime"
