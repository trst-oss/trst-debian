ARG BASE_TAG=latest@sha256:58da1e53437e7ee77f03fd183802ce8cecc897bbd1e20fa8ffa3e166ce9f4d10
FROM registry.gitlab.com/trst-oss/trst-debian:${BASE_TAG}

RUN  dpkg --add-architecture arm64 && \
  echo "deb [arch=amd64,arm64] http://deb.debian.org/debian bookworm main" > /etc/apt/sources.list.d/multistrap-debian.list && \
  echo "deb [arch=amd64,arm64] http://security.debian.org bookworm-security main" > /etc/apt/sources.list.d/multistrap-security.list && \
  apt-get update
